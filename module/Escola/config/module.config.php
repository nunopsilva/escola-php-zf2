<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Escola\Controller\Escola' => 'Escola\Controller\EscolaController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'escola' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/escola[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Escola\Controller\Escola',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'escola' => __DIR__ . '/../view',
        ),
    ),
);
