<?php
 namespace Escola\Infrastructure\DAL;

 use Zend\Db\TableGateway\TableGateway;
 use Escola\Model\Aluno;

 class AlunoTable
 {
     protected $tableGateway;

     public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

     public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }

     public function getAluno($id)
     {
         $id  = (int) $id;
         $rowset = $this->tableGateway->select(array('id' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("Could not find row $id");
         }
         return $row;
     }

     public function saveAluno(Aluno $aluno)
     {
         $data = array(
             'nome' => $aluno->nome,
         );

         $id = (int) $aluno->id;
         if ($id == 0) {
             $this->tableGateway->insert($data);
         } else {
             if ($this->getAluno($id)) {
                 $this->tableGateway->update($data, array('id' => $id));
             } else {
                 throw new \Exception('Aluno id does not exist');
             }
         }
     }

     public function deleteAluno($id)
     {
         $this->tableGateway->delete(array('id' => (int) $id));
     }
 }

?>